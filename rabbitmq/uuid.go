package rabbitmq

import "github.com/google/uuid"

func parseUUID(s string) uuid.UUID {
	id, err := uuid.Parse(s)
	if err != nil {
		return uuid.Nil
	}

	return id
}
