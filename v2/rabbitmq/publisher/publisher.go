package publisher

import (
	"bitbucket.org/labpatoscedro/pubsub/v2"
	"bitbucket.org/labpatoscedro/pubsub/v2/rabbitmq/connection"
	"fmt"
	"sync"
	"time"

	"github.com/streadway/amqp"
)

func New(uri string, options ...Option) pubsub.Publisher {
	pub := &publisher{
		connectionOptions: &connection.ConnectionOptions{URI: uri},
		mutex:             &sync.Mutex{},
	}

	for _, option := range options {
		option(pub)
	}

	return pub
}

type publisher struct {
	conn              connection.Connection
	connectionOptions *connection.ConnectionOptions
	mutex             *sync.Mutex
}

func (p *publisher) Publish(m pubsub.Message, topic string, routingKey ...string) error {
	p.mutex.Lock()
	defer p.mutex.Unlock()

	return p.getConnection().GetChannel().Publish(
		topic,
		p.extractFirstRoutingKeyOrDefault(routingKey),
		false,
		false,
		amqp.Publishing{
			Headers:         m.Headers(),
			ContentType:     m.ContentType(),
			ContentEncoding: m.ContentEncoding(),
			DeliveryMode:    m.DeliveryMode(),
			Priority:        m.Priority(),
			CorrelationId:   m.CorrelationID().String(),
			ReplyTo:         m.ReplyTo(),
			Expiration:      p.getExpirationStringInMillisecondsOrDefault(m.Expiration()),
			MessageId:       m.ID().String(),
			Timestamp:       m.Timestamp(),
			Type:            m.Type(),
			UserId:          m.UserID(),
			AppId:           m.AppID(),
			Body:            m.Body(),
		})
}

func (p *publisher) getConnection() connection.Connection {
	if p.conn == nil {
		p.conn = connection.NewConnection(p.connectionOptions)
	}

	return p.conn
}

func (p *publisher) getExpirationStringInMillisecondsOrDefault(expiration time.Duration) string {
	if expiration > 0 {
		return fmt.Sprintf("%d", expiration.Milliseconds())
	}

	return ""
}

func (p *publisher) extractFirstRoutingKeyOrDefault(key []string) string {
	if len(key) > 0 {
		return key[0]
	}

	return ""
}
