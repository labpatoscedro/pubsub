package publisher

import "github.com/streadway/amqp"

type Option func(*publisher)

func WithEnsureDurableQueue(name string, args map[string]interface{}) Option {
	return func(p *publisher) {
		_, _ = p.getConnection().GetChannel().QueueDeclare(name, true, false, false, false, args)
	}
}

func WithEnsureDurableTopic(name string, args map[string]interface{}) Option {
	return func(p *publisher) {
		_ = p.getConnection().GetChannel().ExchangeDeclare(name, amqp.ExchangeTopic, true, false, false, false, args)
	}
}

func WithEnsureDurableFanout(name string, args map[string]interface{}) Option {
	return func(p *publisher) {
		_ = p.getConnection().GetChannel().ExchangeDeclare(name, amqp.ExchangeFanout, true, false, false, false, args)
	}
}
