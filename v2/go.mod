module bitbucket.org/labpatoscedro/pubsub/v2

go 1.15

require (
	bitbucket.org/labpatoscedro/uuid v1.1.0
	github.com/streadway/amqp v1.0.0
)
