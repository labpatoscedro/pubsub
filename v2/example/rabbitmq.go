package main

import (
	"bitbucket.org/labpatoscedro/pubsub/v2"
	"bitbucket.org/labpatoscedro/pubsub/v2/rabbitmq/publisher"
	"bitbucket.org/labpatoscedro/pubsub/v2/rabbitmq/subscriber"
	"log"
	"time"

	"bitbucket.org/labpatoscedro/pubsub/v2/rabbitmq"
)

func main() {
	go subscriberWithOptions()

	time.Sleep(1 * time.Second)
	myPublisher()
}

func myPublisher() {
	pub := publisher.New("amqp://guest:guest@localhost:5672/")

	for {
		for i := 0; i < 100; i++ {
			_ = pub.Publish(rabbitmq.NewMessage(), "my-exchange")
		}

		time.Sleep(200 * time.Millisecond)
	}
}

func subscriberWithOptions() {
	subs := subscriber.New("amqp://guest:guest@localhost:5672/",
		subscriber.WithDurableFanoutExchange("my-exchange-fanaout"),
		subscriber.WithDurablePriorityQueue("my-priority-queue", 5),
		subscriber.WithPrefetchQos(20, 0, false),
		subscriber.WithDurableFanoutExchange("my-exchange"),
		subscriber.WithDurableTopicExchange("my-topic-exchange", "my key"))

	subs.Subscribe(func(m pubsub.Message) {
		log.Println("wo: consumed message", m.ID())
		time.Sleep(100 * time.Millisecond)
		if err := m.Ack(); err != nil {
			log.Println(err)
		}
	})
}
